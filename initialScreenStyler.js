var initialScreenStyler = {
    elementStyler: elementStyler,

    styleBody: function (body) {
        elementStyler.setBackgroundColor(body, 'rosybrown');
    },

    styleMessagesContainer: function (container) {
        elementStyler.setColor(container, 'white');
        elementStyler.setFontSize(container, '3vw');
        elementStyler.setMargin(container, '5vw');
        elementStyler.setTextAlign(container, 'center');
    },

    styleButtonsContainer: function (container) {
        elementStyler.setMarginLeft(container, '33vw');
    },

    styleMessage: function (message, text) {
        elementStyler.setInnerText(message, text);
    },

    styleButton: function (button, text) {
        elementStyler.setId(button, text);
        text = text.substring(0, 1).toUpperCase() + text.substring(1, text.length - 6).toLowerCase();
        elementStyler.setInnerText(button, text);
        elementStyler.setWidth(button, '11vw');
        elementStyler.setHeight(button, '5vw');
        elementStyler.setColor(button, 'rosybrown');
        elementStyler.setFontSize(button, '1vw');
    },
};