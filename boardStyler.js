var boardStyler = {
    elementStyler: elementStyler,

    styleBoardContainer: function (container) {
        this.elementStyler.setTextAlign(container, 'center');
        this.elementStyler.setMarginTop(container, '10vh');
    },

    styleMessageContainer: function (container, win) {
        this.elementStyler.setInnerText(container, win ? 'Congrats, you won!' : 'You lost!');
        this.elementStyler.setColor(container, 'white');
        this.elementStyler.setFontSize(container, '3vw');
        this.elementStyler.setMargin(container, '5vw');
        this.elementStyler.setTextAlign(container, 'center');
    },

    styleCell: function (cell, disable, hide, surroundingMines) {
        if (disable) {
            this.elementStyler.setDisabled(cell, true);
            return;
        }

        if (hide) {
            this.elementStyler.setVisibility(cell, 'hidden');
            return;
        }

        if (surroundingMines) {
            this.styleNumber(cell, surroundingMines);
            return;
        }

        this.elementStyler.setWidth(cell, '2.5vw');
        this.elementStyler.setHeight(cell, '2.5vw');
        this.elementStyler.setBoxShadow(cell, '10px 10px 10px black');
        this.elementStyler.setColor(cell, '#e6e6e6');
        this.elementStyler.setBackgroundColor(cell, '#e6e6e6');
        this.elementStyler.setFontWeight(cell, 'bold');
        this.elementStyler.setFontSize(cell, '2vw');
        this.elementStyler.setInnerText(cell, '.');
    },

    styleNumber: function (cell, surroundingMines) {
        this.elementStyler.setBackgroundColor(cell, 'rosybrown');
        this.elementStyler.setInnerText(cell, surroundingMines);

        if (surroundingMines === '1')
            this.elementStyler.setColor(cell, 'blue');
        else if (surroundingMines === '2')
            this.elementStyler.setColor(cell, 'green');
        else if (surroundingMines === '3')
            this.elementStyler.setColor(cell, 'red');
        else if (surroundingMines === '4')
            this.elementStyler.setColor(cell, 'darkblue');
        else
            this.elementStyler.setColor(cell, 'black');

    },

    styleMine: function (mine) {
        this.elementStyler.setWidth(mine, '2.5vw');
        this.elementStyler.setHeight(mine, '3.4vh');
        this.elementStyler.setSrc(mine, 'mine-png-8.png');
    },

    styleButton: function (button) {
        this.elementStyler.setInnerText(button, 'Replay');
        this.elementStyler.setWidth(button, '11vw');
        this.elementStyler.setHeight(button, '3.5vw');
        this.elementStyler.setColor(button, 'rosybrown');
        this.elementStyler.setFontSize(button, '1vw');
    }
};