var boardLoader = {
    elementLoader: elementLoader,

    boardBuilder: boardBuilder,

    boardStyler: boardStyler,

    load: function (sizeAndMines) {
        this.loadContainer();
        this.setBoard(sizeAndMines);
        this.loadBoard();
    },

    loadContainer: function () {
        this.elementLoader.load(this, 'boardContainer', document.body, 'div');
        this.boardStyler.styleBoardContainer(this.boardContainer);
    },

    setBoard: function (sizeAndMines) {
        var size = sizeAndMines[0],
            mines = sizeAndMines[1];
        this.board = boardBuilder.build(size, mines);
    },

    loadBoard: function () {
        for (let i = 0; i < this.board.size; i++) {
            this.loadBoardRow(i);
        }
    },

    loadBoardRow: function (i) {
        this.elementLoader.load(this, 'row' + i, this.boardContainer, 'div');

        for (let j = 0; j < this.board.size; j++) {
            this.elementLoader.load(this, i + 'v' + j + 'h', this['row' + i], 'button');
            this.boardStyler.styleCell(this[i + 'v' + j + 'h']);
            this.manageCell(this[i + 'v' + j + 'h'], i, j);
        }
    },

    manageCell: function (cell, i, j) {
        var self = this;
        cell.addEventListener('click', function () {
            if (self.board.cellIsMine(i, j)) {
                self.terminateGame();
            } else {
                self.board.generateNextState(i, j);
                self.loadBoardNextState();

                if (self.board.allCellsAreRevealed()) {
                    self.terminateGame(true);
                }
            }
        })
    },

    terminateGame: function (win) {
        this.showMines();
        this.disableBoard();
        this.loadMessage(win);
        this.loadButton();
    },

    showMines: function () {
        for (let i = 0; i < this.board.minesIndexes.length; i++) {
            let cell = this[this.board.minesIndexes[i][0] + 'v' + this.board.minesIndexes[i][1] + 'h'];
            this.showMine(cell, i);
        }
    },

    showMine: function (cell, i) {
        this.elementLoader.create(this, 'mine' + i, 'img');
        this.elementLoader.replace(cell.parentNode, this['mine' + i], cell);
        this.boardStyler.styleMine(this['mine' + i],);
    },

    disableBoard: function () {
        for (let i = 0; i < this.board.size; i++) {
            for (let j = 0; j < this.board.size; j++) {
                if (this[i + 'v' + j + 'h']) {
                    this.boardStyler.styleCell(this[i + 'v' + j + 'h'], true);
                }
            }
        }
    },

    loadMessage: function (win) {
        this.elementLoader.load(this, 'messageContainer', this.boardContainer, 'div');
        this.boardStyler.styleMessageContainer(this.messageContainer, win);
    },

    loadButton: function () {
        this.elementLoader.load(this, 'replayButton', this.messageContainer, 'button');
        this.boardStyler.styleButton(this.replayButton);
        this.replayButton.addEventListener('click', function () {
            location.reload();
        })
    },

    loadBoardNextState: function () {
        for (let i = 0; i < this.board.size; i++) {
            for (let j = 0; j < this.board.size; j++) {
                if (this.mustChangeCellValue(i, j)) {
                    this.mustHideCell(i, j) ? this.hideCell(i, j) : this.showNumberOfSurroundingMines(i, j);
                }
            }
        }
    },

    mustChangeCellValue: function (i, j) {
        return this.board.visibleGrid[i][j] !== '-';
    },

    mustHideCell: function (i, j) {
        return this.board.visibleGrid[i][j] === ' ';
    },

    hideCell: function (i, j) {
        this.boardStyler.styleCell(this[i + 'v' + j + 'h'], false, true);
    },

    showNumberOfSurroundingMines(i, j) {
        var surroundingMines = this.board.visibleGrid[i][j];
        this.boardStyler.styleCell(this[i + 'v' + j + 'h'], false, false, surroundingMines);
    }
};