var boardBuilder = {
    build: function (size, mines) {
        return {
            backingGrid: this.createGrid(size, mines),
            visibleGrid: this.createGrid(size),
            size: size,
            minesIndexes: this.minesIndexes,
            generateNextState: this.generateNextState,
            allCellsAreRevealed: this.allCellsAreRevealed,
            cellIsMine: this.cellIsMine
        }
    },

    createGrid: function (size, mines) {
        var grid = [];
        for (let i = 0; i < size; i++) {

            grid[i] = [];
            for (let j = 0; j < size; j++) {
                grid[i][j] = '-';
            }
        }

        if (mines)
            this.addMines(grid, mines);

        return grid;
    },

    addMines: function (grid, mines) {
        this.minesIndexes = [];

        var minesPosted = 0;
        while (minesPosted < mines) {
            let i = this.generateRandomIndex(grid.length);
            let j = this.generateRandomIndex(grid.length);

            if (!this.isMineAlreadyPosted(grid, i, j)) {
                this.postMine(grid, i, j);
                this.minesIndexes.push([i, j]);
                minesPosted++;
            }
        }
    },

    generateRandomIndex: function (size) {
        return Math.floor(Math.random() * size);
    },

    isMineAlreadyPosted: function (grid, i, j) {
        return grid[i][j] === '*';
    },

    postMine: function (grid, i, j) {
        grid[i][j] = '*';
    },

    generateNextState: function (i, j) {
        if (this.visibleGrid[i][j] === '-') {
            let surroundingMinesCounter = 0;

            let left;
            if (j > 0) {
                left = this.backingGrid[i][j - 1];
                if (left === '*') surroundingMinesCounter++;
            }

            let leftDown;
            if (j > 0 && i < this.size - 1) {
                leftDown = this.backingGrid[i + 1][j - 1];
                if (leftDown === '*') surroundingMinesCounter++;
            }

            let down;
            if (i < this.size - 1) {
                down = this.backingGrid[i + 1][j];
                if (down === '*') surroundingMinesCounter++;

            }

            let rightDown;
            if (j < this.size - 1 && i < this.size - 1) {
                rightDown = this.backingGrid[i + 1][j + 1];
                if (rightDown === '*') surroundingMinesCounter++;
            }

            let right;
            if (j < this.size - 1) {
                right = this.backingGrid[i][j + 1];
                if (right === '*') surroundingMinesCounter++;
            }

            let rightUp;
            if (j < this.size - 1 && i > 0) {
                rightUp = this.backingGrid[i - 1][j + 1];
                if (rightUp === "*") surroundingMinesCounter++;
            }

            let up;
            if (i > 0) {
                up = this.backingGrid[i - 1][j];
                if (up === "*") surroundingMinesCounter++;
            }

            let leftUp;
            if (i > 0 && j > 0) {
                leftUp = this.backingGrid[i - 1][j - 1];
                if (leftUp === "*") surroundingMinesCounter++;
            }

            if (surroundingMinesCounter > 0)
                this.visibleGrid[i][j] = '' + surroundingMinesCounter;
            else {
                this.visibleGrid[i][j] = ' ';

                if (left !== undefined) this.generateNextState(i, j - 1);
                if (leftDown !== undefined) this.generateNextState(i + 1, j - 1);
                if (down !== undefined) this.generateNextState(i + 1, j);
                if (rightDown !== undefined) this.generateNextState(i + 1, j + 1);
                if (right !== undefined) this.generateNextState(i, j + 1);
                if (rightUp !== undefined) this.generateNextState(i - 1, j + 1);
                if (up !== undefined) this.generateNextState(i - 1, j);
                if (leftUp !== undefined) this.generateNextState(i - 1, j - 1);
            }
        }
    },

    allCellsAreRevealed: function () {
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                let visibleGridCellValue = this.visibleGrid[i][j];
                let backingGridCellValue = this.backingGrid[i][j];

                if (visibleGridCellValue === '-' && backingGridCellValue !== '*') {
                    return false;
                }
            }
        }
        return true;
    },

    cellIsMine: function (i, j) {
        return this.backingGrid[i][j] === '*';
    }
};