var game = {
    initialScreenLoader: initialScreenLoader,

    boardLoader: boardLoader,

    start: function () {
        this.initialScreenLoader.load();
        this.waitForClick();
    },

    waitForClick: function () {
        var self = this;
        this.initialScreenLoader.click
            .then(function (sizeAndMines) {
                self.initialScreenLoader.unload();
                self.boardLoader.load(sizeAndMines);
            });
    }
};