var elementStyler = {
    setBackgroundColor: function (element, value) {
        element.style.backgroundColor = value;
    },

    setFontSize: function (element, value) {
        element.style.fontSize = value;
    },

    setMargin: function (element, value) {
        element.style.margin = value;
    },

    setTextAlign: function (element, value) {
        element.style.textAlign = value;
    },

    setMarginLeft: function (element, value) {
        element.style.marginLeft = value;
    },

    setId: function (element, value) {
        element.id = value;
    },

    setInnerText: function (element, value) {
        element.innerText = value;
    },

    setWidth: function (element, value) {
        element.style.width = value;
    },

    setHeight: function (element, value) {
        element.style.height = value;
    },

    setColor: function (element, value) {
        element.style.color = value;
    },

    setMarginTop: function (element, value) {
        element.style.marginTop = value;
    },

    setBoxShadow: function (element, value) {
        element.style.boxShadow = value;
    },

    setFontWeight: function (element, value) {
        element.style.fontWeight = value;
    },

    setSrc: function (element, value) {
        element.src = value;
    },

    setVisibility: function (element, value) {
        element.style.visibility = value;
    },

    setDisabled: function (element, value) {
        element.disabled = value;
    }
};