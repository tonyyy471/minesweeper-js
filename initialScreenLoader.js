var initialScreenLoader = {
    elementLoader: elementLoader,

    initialScreenStyler: initialScreenStyler,

    load: function () {
        this.loadBody();
        this.loadMessages();
        this.loadButtons();
    },

    unload: function () {
        this.elementLoader.unload(this, 'messagesContainer');
        this.elementLoader.unload(this, 'buttonsContainer');
    },

    loadBody: function () {
        this.initialScreenStyler.styleBody(document.body);
    },

    loadMessages: function () {
        this.loadContainer('messagesContainer', 'div');
        this.loadMessage('minesweeper', 'h4', 'Minesweeper');
        this.loadMessage('choose_difficulty', 'h6', 'Choose difficulty:');
    },

    loadButtons: function () {
        this.loadContainer('buttonsContainer', 'div');
        this.loadButton('beginnerButton', 9, 10);
        this.loadButton('intermediateButton', 16, 40);
        this.loadButton('advancedButton', 24, 99);
        this.loadClickPromise();
    },

    loadContainer: function (containerName, tagName) {
        elementLoader.load(this, containerName, document.body, tagName);
        containerName === 'messagesContainer' ? this.initialScreenStyler.styleMessagesContainer(this.messagesContainer) :
            this.initialScreenStyler.styleButtonsContainer(this.buttonsContainer);
    },

    loadMessage: function (messageName, tagName, text) {
        elementLoader.load(this, messageName, this.messagesContainer, tagName);
        this.initialScreenStyler.styleMessage(this[messageName], text);
    },

    loadButton: function (buttonName) {
        elementLoader.load(this, buttonName, this.buttonsContainer, 'button');
        this.initialScreenStyler.styleButton(this[buttonName], buttonName);
    },

    loadClickPromise: function () {
        var resolveOnEvent = this.resolveOnEvent;

        this.click = new Promise(function (resolve) {
            resolveOnEvent('click', 'beginnerButton', resolve, 9, 10);
            resolveOnEvent('click', 'intermediateButton', resolve, 16, 40);
            resolveOnEvent('click', 'advancedButton', resolve, 24, 99);
        });
    },

    resolveOnEvent: function (type, element, resolve, ...args) {
        this[element].addEventListener(type, function () {
            resolve(args);
        });
    },
};
