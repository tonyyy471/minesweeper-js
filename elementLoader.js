var elementLoader = {
    load: function (context, elementName, parent, tagName) {
        this.create(context, elementName, tagName);
        this.append(context[elementName], parent);
    },

    unload: function (context, elementName) {
        this.delete(context, elementName);
    },

    create: function (context, elementName, tagName) {
        context[elementName] = document.createElement(tagName);
    },

    delete: function (context, elementName) {
        context[elementName].remove();
    },

    append: function (child, parent) {
        parent.appendChild(child);
    },

    replace: function (parent, newChild, oldChild) {
        parent.replaceChild(newChild, oldChild);
    }
};